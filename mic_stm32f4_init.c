#include "stm32f4_discovery.h"
#include "stm32f4_discovery_audio_codec.h"
#include "stm32f4xx.h"
#include "wave_recorder.h"
#include "wave_player.h"
#include "interrupt_handlers.h"
#include "board_communication.h"
#include "stm32f4xx_adc.h"

unsigned int soundEffectNum = 0;
unsigned int masterorslave = 1; //init as master

// SysTick Handler (ISR for the System Tick interrupt)
void SysTick_Handler(void){
  msTicks++;
}

// Initialize the system tick 
void InitSystick(void){
	SystemCoreClockUpdate();                      
	if (SysTick_Config(SystemCoreClock / 1000)) { 
		while (1);                             
	}
}

void initADC(){
	// In this function, we'll set up the ADC and use 
	// it to read values from an on-board temperature sensor.

        // Refer to the ADC library header and source files
        // (stm32f4xx_adc.h and stm32f4xx_adc.c) for more details on this code.

	// Declare adc as a ADC_InitTypeDef, adc_common as a ADC_CommonInitTypeDef
	ADC_InitTypeDef adc;
	ADC_CommonInitTypeDef adc_common;
	
	// Enable clock signal to ADC peripheral
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

	
	// Common ADC Initialization
	ADC_CommonStructInit(&adc_common); // initialize struct to default values
        // Change a few values
	adc_common.ADC_Prescaler = ADC_Prescaler_Div8; // sets ADC clock prescaler
	ADC_CommonInit(&adc_common);

        // ADC initialization
	ADC_StructInit(&adc);  	// initialize struct to default values

	// TODO: explicitly set some values before initialization
	// Set the ADC resolution to 12 bits
	ADC_InitTypeDef c;
	c.ADC_Resolution = ADC_Resolution_12b;
	// and enable continuous conversion mode
	c.ADC_ContinuousConvMode = ENABLE;



	ADC_Init(ADC1, &adc);
	// ADC Channel 1 configuration
	ADC_RegularChannelConfig(ADC1, ADC_Channel_TempSensor, 1, ADC_SampleTime_144Cycles);
	
	// Enable temperature smensor
	ADC_TempSensorVrefintCmd(ENABLE);

	// Enable ADC
	ADC_Cmd(ADC1, ENABLE);


}

int main(){
	STM_EVAL_LEDInit(LED4);
	STM_EVAL_LEDInit(LED3);
	STM_EVAL_LEDInit(LED5);
	STM_EVAL_LEDInit(LED6);
	STM_EVAL_LEDOn(LED4);
	//configuring user button to respond to interrupts which is speak-mode and listen-mode
	STM_EVAL_PBInit(BUTTON_USER,BUTTON_MODE_EXTI);
	
	//No data to play initially
	Data_Status = 0;
	
	//configure the mic/audio recording
	WaveRecorderInit (I2S_AudioFreq_32k,16, 1);
	
	//configure audio jack for playback
	WavePlayerInit(I2S_AudioFreq_16k);
	
	//Enable mic peripheral to receive data. Will always be enabled for real time record and play
	I2S_Cmd(SPI2, ENABLE);
	
	//configure SPI1
	init_SPI1();
	

	//wait until there is data to play
	while (!BUTTON_USER);
	WavePlayBack();
	//record audio when button is pressed
	while (BUTTON_USER)
	

	//keep program running
	while(1){

		
	}
	
}



void setup()
{
    // Pin 0 (PA0) is the User button. 
    // We use it as PTT
    pinMode(USER_BUTTON, INPUT);

    // Pin 60 (PD12, LED4) is the Green 'initialised OK' LED
    pinMode(INIT_OK_LED, OUTPUT);
    digitalWrite(INIT_OK_LED, LOW);

    // Pin 62 (PD14, LED5) is the Red TRansmit LED
    pinMode(TRANSMIT_LED, OUTPUT);
    digitalWrite(TRANSMIT_LED, LOW);

    // Disable the accelerometer, set its CS (pin 67, PE3) high
    // otherwise the accelerometer output to PA6 will interfere with the Radio MISO
    pinMode(ACCELEROMETER_CS, OUTPUT);
    digitalWrite(ACCELEROMETER_CS, HIGH);

#ifndef LOOPBACK_TEST
    // Initialise and configure the driver
    if (driver.init())
	digitalWrite(INIT_OK_LED, HIGH);
    else
	printf("Driver init failed\n");
#if defined(USE_DRIVER_RF22)
    // For RF22, Defaults after init are 434.0MHz, 0.05MHz AFC pull-in, modulation FSK_Rb2_4Fd36
    // but thats too slow: air time for each message must be < 20ms, else we cant keep up with the packet rate
    // Fast, wide modulations with short air times per packet 
    // may let us interleave multiple transmitters on the same frequency, perhaps with
    // different destination addresses configured into RadioHead driver.
    driver.setModemConfig(RH_RF22::GFSK_Rb125Fd125); // good, but overkill?
//    driver.setModemConfig(RH_RF22::GFSK_Rb9_6Fd45); // Very choppy
//    driver.setModemConfig(RH_RF22::GFSK_Rb19_2Fd9_6); // Choppy
    // Could also configure Radio frequency here
    // Could configure RadioHead driver addresses, etc here for private(ish) networks etc
#elif defined(USE_DRIVER_SERIAL)
    Serial2.begin(9600);
#endif

#endif

    // mic_fifo holds samples received by the microhone
    // spkr_fifo holds samples to be output to the speaker
    // We process 160 x 16 bit samples at a time (20ms), so 320 samples gives us double buffering
    mic_fifo = fifo_create(320);
    spkr_fifo = fifo_create(320);

    // Codec2 Mode 3200 encodes 160 shorts into 64 bits (8 bytes)
    c2 = codec2_create(CODEC2_MODE_3200);

    // Codec2 requires 8000 16 bit samples per sec
    // Minimum clock speed for MEMS mic is 1MHz => 1000000/16 = 62500 16 bit samples/per sec
    // so we choose AudioFreq of 100000 (=> 1024kHz I2S clock => 64k 16 bit samples per sec) and a decimation of 8 
    // to get 8k 16 bit samples per sec.
    if (MicrophoneInit(100000))
	printf("MicrophoneInit failed\n");

    // Output stream requires 8k x 16bits per second. EVAL_AUDIO_Play only supports stereo :-(
    // 8000*16*2 = 256kHz I2C clock
    // so we choose AudioFreq of 100000 (=> 256kHz I2C clock)
    if (SpeakerInit(25000)) // 8k x 16 bit samples per second, stereo = 256kHz
	printf("SpeakerInit failed\n");
	
    // Start the microphone input interrupts. It will insert samples into mic_fifo as they become available.
    MicrophoneStart(mic_fifo);

    // Start the speaker output DMA. It will read samples in batches of 160 from spkr_fifo as we push them in.
    SpeakerStart(spkr_fifo);
}

void loop()
{
#if LOOPBACK_TEST
    // Internal testing code: take mic input, C2 encode it, C2 decode it and output to speaker
    // No RadioHead driver or radio hardware required
    if (digitalRead(USER_BUTTON))
    {
	digitalWrite(TRANSMIT_LED, HIGH); // Tx LED on
	// PTT is pressed: Transmit mode
	while (digitalRead(USER_BUTTON))
	{
	    short buffer[160];
	    
	    if (fifo_read(mic_fifo, buffer, 160) == 0)
	    {
		// 160 shorts available from the microphone, encode them
		unsigned char bits[8];
		codec2_encode(c2, bits, buffer);
		// Now decode them
		codec2_decode(c2, buffer, bits);
		// And output what should be the same speech as the original input
		fifo_write(spkr_fifo, buffer, 160);
	    }
	}
	digitalWrite(TRANSMIT_LED, LOW); // Tx LED off
    }

#else
    // Implement a walkie talkie with PTT over a RadioHead radio or Serial port driver
    if (digitalRead(USER_BUTTON))
    {
	digitalWrite(TRANSMIT_LED, HIGH); // Tx LED on
	// PTT is pressed: Transmit mode
	while (digitalRead(USER_BUTTON))
	{
	    short buffer[160];
	    
	    if (fifo_read(mic_fifo, buffer, 160) == 0)
	    {
		// 160 shorts available from the microphone, encode them to 8 bytes
		uint8_t bits[8]; // big enough for any C2 encoded frame
		int bytes_per_frame = (codec2_bits_per_frame(c2) + 7) / 8;
                // This takes about 6.5ms:
		codec2_encode(c2, bits, buffer);
		// Wait for any previous packet to be sent
		driver.waitPacketSent();
		// Now transmit the 8 encoded bytes
		driver.send(bits, bytes_per_frame);
	    }
	}
	digitalWrite(TRANSMIT_LED, LOW); // Tx LED off
    }
    else
    {
	// PTT is released: Receive mode
	// Wait for any previous packet to be sent
	driver.waitPacketSent();
	while (!digitalRead(USER_BUTTON))
	{
	    uint8_t bits[8];// big enough for any C2 encoded frame
	    uint8_t len = sizeof(bits);
	    int bytes_per_frame = (codec2_bits_per_frame(c2) + 7) / 8;
	    if (driver.recv(bits, &len)
		&& len == bytes_per_frame)
	    {
		// Got encoded data, decode it
		short buffer[160];
		codec2_decode(c2, buffer, bits);

		// And output what should be the same speech as the original input on the transmitter side
		fifo_write(spkr_fifo, buffer, 160);
	    }
     	}
    }

#endif

}
