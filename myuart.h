#ifndef _MY_UART_
#define _MY_UART_
/*
*  DESCRIPTION:      This library uses UART2
*	 AURTHOR: 		     ###$$$    ***srikanth***    $$$###
*	 LAST EDITED DATE: 16/04/2014 
*
*/
#define MY_BAUD_RATE 9600

void my_uart_init_uart2(int baud_rate);
void my_uart_send_data_uart2( unsigned char data);



char my_uart_read_data_uart2(void);

void my_uart_init_uart1(int baud_rate);
void my_uart_send_data_uart1( unsigned char data);



char my_uart_read_data_uart1(void);

#endif
