# This document is an extract of the original report submitted by me in 2011 #

=======================================================================================================================================================================

> The following image is to establish that this is indeed my work.


![](http://s9.postimg.org/b2qxujv67/IMG_20160128_135725_1.jpg "certification")


## Simple display of characters ##

>The following images contain the baremetal and low level code in assembly which incudes the firmware for operating the LCD display


![](http://s28.postimg.org/yoyqcr2lp/IMG_20160128_135802.jpg)


![](http://s15.postimg.org/a0y650vnv/IMG_20160128_135816.jpg)


> The next image is a flowchart corresponding to the approach for developing the software


![](http://s18.postimg.org/e8xeyqugp/IMG_20160128_135826.jpg)


## Rolling stopping and flashing of characters ##

>The following images also contain the baremetal and low level code in assembly which incudes the firmware for operating the LCD display, but for a different functionality


![](http://s9.postimg.org/6u51uq2dr/IMG_20160128_135845.jpg)


![](http://s8.postimg.org/hez0fvxol/IMG_20160128_135856.jpg)


![](http://s15.postimg.org/5wfacxn4b/IMG_20160128_135908.jpg)


![](http://s14.postimg.org/ja4pj5lg1/IMG_20160128_135923.jpg)


> The next image is a flowchart corresponding to the approach for developing the software


![](http://s24.postimg.org/4djr2wq39/IMG_20160128_135935.jpg)


## Images of the test board, LCD monitor and other equipment used: ##


![](http://s7.postimg.org/z0zuja263/IMG_20160128_140028.jpg)


![](http://s11.postimg.org/we19q229f/IMG_20160128_140019.jpg)


> The following picture shows the pin architecture of the microcontriller used, please note that i have made use of only the 8051 microcontroller.


![](http://s29.postimg.org/z9y0r0xhz/IMG_20160128_140542.jpg)

==================================================================================================================================================================